# Guardian US quotes template

-----------------------

## What you want to do:

Create an interactive like this example: [ 'I could have been Mike Brown': your stories of racial profiling by the world's police ](http://www.theguardian.com/commentisfree/ng-interactive/2014/aug/29/-sp-mike-brown-stories-racial-profiling-police)

## How to do that:

Making a quotes interactive for your reader responses is easier that you might have imagine thanks to our template. *No coding required!* All you have to do is wrangle a spreadsheet and a URL. 

Here's the seven-step process:

#### 1. Click on [ this link ](https://docs.google.com/spreadsheets/d/1URLmgSsThh4zxTZaINnL1NwofX51dxh36O7MWWxuHjc/edit?usp=sharing) in order to see our sample spreadsheet

* Click "File --> Make a copy" and then re-name your new spreadsheet when prompted to something fitting your topic (it can be anything you want, it won't affect the project)


#### 2. Congrats, you have a spreadsheet. There are two tabs at the bottom of it, and you'll need to fill in some different info for both. The key thing NOT TO DO is change the column headers of either sheet. Otherwise, edit away.

* Responses sheet:
	+ Under the "Quote" header is where you'll be putting all your pull-quotes. Those are the juicy quotes from each respondant you particularly want to feature. These are the always-visible quotes in the Ferguson example. These should be limited to ONE or TWO sentences.
	+ "Full Text" will be the rest of the response. It's the text that shows when you click the "Read more" button
		- In order to create a clean line-break within this text you'll want to add <br><br> (double breaks)
		- You can make text italic by adding normal italics tags to it. Ex. <i>Wow, now I'm slatny.</i>
		- You can make text bold by adding <span> tags. Ex. <span>This word is now bold and special.</span>
	+ The "Attribution" column will allow you to attribute the quote. You can format this however you want (First name, last name, age, location, etc.)
	+ "Image Url" is where you put any images you want to embed among the quotes. In the linked example, those are the cutout portraits. Respondents provided those to the Cif editors, who then had them cut out on a white background. These images should be 250px wide, but height can vary.  
		- You will need to upload the images to the Guardian site, and then get their URLs to paste in here. You can do this using the Batch Uploader tool on your computer. When uploading an image this way, the program will spit back the URL. Usually something like http://static.guim.co.uk/sys-images/Guardian/Pix/pictures/2014/8/29/1409318245816/image.jpeg (that's a dud link)
	+ "Image Position" should only be filled in for those quotes that have Image Urls, and otherwise be left blank. 
		- Giving an image position "right" will make it appear to the right of the quote
		- Giving an image position "left" will make it appear to the left of the quote
		- Leaving the field blank means no image will be placed
* Header sheet:
	+ "Main image" will be where you paste the url to the banner image that goes at the top of the page. Our preference is for a 1300px-wide, narrow banner, like the one in the Ferguson example. This will be uploaded in the same way as the portrait images.
	+ "accentcolor" is where you can change the color of the quotes and the "Read more" and "Hide" buttons. Colors are sex via hex codes. We built this in so that those accent pieces can be matched to the art or to the Guardian section color. (See example Guardian colors with corresponding hex codes [ here ](https://drive.google.com/open?id=0B99TOrEhlZ9RODZKa0U5Y1R1NGdiVUFOeC1za1pqekN6Q3pz&authuser=0) and [ here ](http://guardian.github.io/pasteup-palette/))
	+ "credit" is an extra section at the bottom of the interactive. It can be used to add image credits, extra reporting credits, other links, etc. Or it can be left blank.


#### 3. When everything is all set up in the sheets, you'll want to publish the project. Click "File --> Publish to web"
* Choose "Entire document" from the drop-down, and click "Publish" – this will give you a link much like this one "https://docs.google.com/spreadsheets/d/1m4ewgtI9C8GwHO8EPn2uzaUAcHWFwcBDDWuwmTyLCYc/pubhtml"
* That big string of numbers after the /d/ and before the /pubhtml is your key. You're going to need this part. Copy it and save it somewhere temporarily. Example key: 1m4ewgtI9C8GwHO8EPn2uzaUAcHWFwcBDDWuwmTyLCYc


#### 4. Email someone on the interactive team (UK or US) your key. 

* The interactive team member will need to add that project's key to a special spreadsheet only we can access that controls publishing/hosting this whole thing on our site. 


#### 5. Now open a new tab. Go to [ this link ](http://interactive.guim.co.uk/embed/2014/08/quotes-template/index.html) – that's where the quotes template lives
* When you open the link, it should just say "loading..." until it loads


#### 6. Go get your key. Type in ?key=YOURKEY at the end of the link you just opened. It'll look like this (Ferguson example): http://interactive.guim.co.uk/embed/2014/08/quotes-template/index.html?key=1m4ewgtI9C8GwHO8EPn2uzaUAcHWFwcBDDWuwmTyLCYc <-- notice it is now populated, like magic


#### 7. You're going to take that _entire_ link. The whole thing: http://interactive.guim.co.uk/embed/2014/08/quotes-template/index.html?key=1m4ewgtI9C8GwHO8EPn2uzaUAcHWFwcBDDWuwmTyLCYc. And paste it into a Composer interactive page.

* Open Composer. Click "Create new --> Interactive"
* Click on the < .. > section and it'll open up a box that says "Paste URL to embed media". That is where you paste the URL to embed your quotes project. 
* Paste your URL. 
* Add all the furniture you need, and then preview. Everything should be working fine, publish away.