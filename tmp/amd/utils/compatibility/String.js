define(function () {

	'use strict';
	
	if ( !String.prototype.trim ) {
		String.prototype.trim = function () {
			return this.replace( /^\s+/, '' ).replace( /\s+$/, '' );
		}
	}

});