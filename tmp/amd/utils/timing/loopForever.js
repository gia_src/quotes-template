define(['utils/compatibility/requestAnimationFrame'],function (requestAnimationFrame) {

	'use strict';
	
	return function loopForever ( callback ) {
		var loop = function(){
			requestAnimationFrame( loop );
			callback();
		};
	
		loop();
	};

});