define(function () {

	'use strict';
	
	var supported = ( 'ontouchstart' in document.documentElement );
	return supported;

});