define([
	"data",
	"require",
	"ractive"
], function(
	__import0__,
	require,
	Ractive
){
var __options__ = {
	template: {v:1,t:[{t:4,r:"top",f:[{t:7,e:"div",a:{"class":"gia-header-img"},f:[{t:7,e:"img",a:{src:[{t:2,r:"mainimage"}]}}]}]},{t:7,e:"div",a:{"class":"gia-quotes"},f:[{t:4,n:52,r:"quotes",i:"i",f:[{t:7,e:"div",a:{"class":["person-row row-",{t:2,r:"rowNumber"}]},f:[{t:4,x:{r:["imageposition"],s:"${0}==\"left\""},f:[{t:7,e:"div",a:{"class":["body-img ",{t:2,r:"imageposition"},"-pos"]},f:[{t:7,e:"img",a:{src:[{t:2,r:"imageurl"}]}}]}]}," ",{t:7,e:"div",a:{"class":"body-text"},f:[{t:4,r:"top",f:[{t:7,e:"svg",a:{x:"0px",y:"0px",width:"50px"},f:[{t:7,e:"path",a:{style:["fill:",{t:2,r:"accentcolor"}],d:"M0,13.409C0,3.771,9.742,0.175,20.741,0v1.605C11.277,2.2,5.312,7.122,5.556,13.723\n\t\t\t\t\t\t\tc1.221-2.652,4.011-5.237,9.284-5.237c5.516,0,9.113,2.341,9.113,7.89c0,5.447-4.783,8.519-11.17,8.519\n\t\t\t\t\t\t\tC4.889,24.895,0,20.425,0,13.409z M26.047,13.409c0-9.638,9.741-13.233,20.741-13.409v1.605C37.361,2.2,31.355,7.122,31.6,13.723\n\t\t\t\t\t\t\tc1.225-2.652,4.014-5.237,9.285-5.237c5.518,0,9.115,2.341,9.115,7.89c0,5.447-4.783,8.519-11.172,8.519\n\t\t\t\t\t\t\tC30.938,24.895,26.047,20.425,26.047,13.409z"}}]}]}," ",{t:7,e:"div",a:{"class":["main-quote quote-",{t:2,r:"./rowNumber"}]},f:[{t:3,r:"quote"}," ",{t:4,n:50,rx:{r:"opened",m:[{t:30,n:"i"}]},f:[{t:4,r:"top",f:[{t:7,e:"span",a:{"class":"less-button","data-id":["my-",{t:2,r:"rowNumber"}],style:["color:",{t:2,r:"accentcolor"}]},v:{click:{n:"closeme",d:[{t:2,r:"i"}]}},f:["Hide"]}]}]},{t:4,n:51,f:[{t:4,r:"top",f:[{t:7,e:"span",a:{"class":"more-button","data-id":["my-",{t:2,r:"rowNumber"}],style:["color:",{t:2,r:"accentcolor"}]},v:{click:{n:"opensesame",d:[{t:2,r:"i"}]}},f:["Read more »"]}]}],rx:{r:"opened",m:[{t:30,n:"i"}]}}]}," ",{t:4,rx:{r:"opened",m:[{t:30,n:"i"}]},f:[{t:7,e:"div",a:{"class":"divHidden","data-response":["MyDiv-",{t:2,r:"rowNumber"}]},t1:"fade",f:[{t:7,e:"div",a:{"class":"hidden-quote"},f:[{t:3,r:"fulltext"}]}," ",{t:7,e:"span",a:{"class":"descrip-name"},f:[{t:2,r:"attribution"}]}]}]}]}," ",{t:4,x:{r:["imageposition"],s:"${0}==\"right\""},f:[{t:7,e:"div",a:{"class":["body-img ",{t:2,r:"imageposition"},"-pos"]},f:[{t:7,e:"img",a:{src:[{t:2,r:"imageurl"}]}}]}]}]}]},{t:4,n:51,f:[{t:7,e:"p",f:["loading..."]}],r:"quotes"}]}]},
	css:".gia-quotes{margin:0;float:left}.gia-header-img{width:100%;position:relative}.person-row{display:table;position:relative;width:100%;float:left;font-family:'Guardian Text Egyptian Web',Georgia,serif;font-size:24px;line-height:1.5em;border-top:1px solid #CCC;margin:5px 0 0;padding:10px 0 0}.body-text{display:table-cell}svg{display:inline-block;position:absolute;top:45px;margin-left:50px}.main-quote{display:inline-block;height:auto;float:left;text-align:left;margin:20px 35px 0 70px;padding:16px 0 16px 50px}.main-quote span{text-align:right;overflow:auto;font-size:18px;padding-top:5px;margin-left:3px;font-family:'Guardian Egyptian Web';font-weight:900;text-decoration:none;background-color:#fff}.main-quote span:hover{text-decoration:underline;cursor:pointer}.divHidden{width:100%;height:auto}.divVisible{display:block;width:900px;float:left;padding-left:40px;padding-bottom:20px}.hidden-quote{float:left;margin:35px 25px 10px 110px}.descrip-name{display:inline-block;color:#929497;font-family:'Guardian Egyptian Web';font-weight:900;font-size:18px;line-height:1.2em;margin:0 25px 25px 110px;float:right}.body-img{position:relative;display:table-cell;margin:0 auto;height:20px;width:25%;max-width:250px;vertical-align:top}.body-img img{width:100%}@media screen and (max-width:500px){.body-img{display:none}.body-text{width:100%}svg{margin-left:10px}.main-quote{margin-left:30px}.hidden-quote{margin-left:10px}.descrip-name{margin-left:10px;float:left}}",
},
component={},
__prop__,
__export__;


// var data = require( 'data' );
// var quote = data.responses.sheets.Responses;
// var fill = data.responses.sheets.Header;



component.exports = {
		init: function(){
			this.on({
			  opensesame: function ( event, index ) {
			    	//this.set( 'opened[*]', false );
			    	this.set( 'opened[' + index + ']', true );
			  },
			   closeme: function ( event, index ){
			    	this.set( 'opened[' + index + ']', false );
			   }
			})
		},
		data: {
			// quotes: quotes,
			// top: fill,
			opened: []
		}
};

  if ( typeof component.exports === "object" ) {
    for ( __prop__ in component.exports ) {
      if ( component.exports.hasOwnProperty(__prop__) ) {
        __options__[__prop__] = component.exports[__prop__];
      }
    }
  }

  __export__ = Ractive.extend( __options__ );
return __export__;
});