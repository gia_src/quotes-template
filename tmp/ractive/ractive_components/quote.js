define([
	"require",
	"ractive"
], function(
	require,
	Ractive
){
var __options__ = {
	template: {v:1,t:[{t:7,e:"div",a:{"class":["person-row row-",{t:2,r:"rowNumber"}]},f:[{t:4,x:{r:["imageposition"],s:"${0}==\"left\""},f:[{t:7,e:"div",a:{"class":["body-img ",{t:2,r:"imageposition"},"-pos"]},f:[{t:7,e:"img",a:{src:[{t:2,r:"imageurl"}]}}]}]}," ",{t:7,e:"div",a:{"class":"body-text"},f:[{t:7,e:"div",a:{"class":["main-quote quote-",{t:2,r:"./rowNumber"}]},f:[{t:3,r:"quote"}," ",{t:4,r:"clicked",f:[{t:7,e:"span",a:{"class":"more-button","data-id":["my-",{t:2,r:"rowNumber"}]},v:{click:{n:"opensesame",d:[{t:2,r:"."}]}},f:["Read more »"]}],n:51}," ",{t:4,r:"clicked",f:[{t:7,e:"span",a:{"class":"less-button","data-id":["my-",{t:2,r:"rowNumber"}]},v:{click:{n:"closeme",d:[{t:2,r:"."}]}},f:["Hide"]}]}]}," ",{t:4,r:"clicked",i:"i",f:[{t:7,e:"div",a:{"class":"divHidden","data-response":["MyDiv-",{t:2,r:"rowNumber"}]},t1:"fade",f:[{t:7,e:"div",a:{"class":"hidden-quote"},f:[{t:3,r:"fulltext"}]}," ",{t:7,e:"span",a:{"class":"descrip-name"},f:[{t:2,r:"attribution"}]}]}]}]}," ",{t:4,x:{r:["imageposition"],s:"${0}==\"right\""},f:[{t:7,e:"div",a:{"class":["body-img ",{t:2,r:"imageposition"},"-pos"]},f:[{t:7,e:"img",a:{src:[{t:2,r:"imageurl"}]}}]}]}]}]},
},
component={},
__prop__,
__export__;

	component.exports = {
		data: {
			clicked: false
		},

		init: function () {
			this.on({
				opensesame: function () {
					this.set( 'clicked', true );
				},
				closeme: function () {
					this.set( 'clicked', false );
				}
			})
		}
	}

  if ( typeof component.exports === "object" ) {
    for ( __prop__ in component.exports ) {
      if ( component.exports.hasOwnProperty(__prop__) ) {
        __options__[__prop__] = component.exports[__prop__];
      }
    }
  }

  __export__ = Ractive.extend( __options__ );
return __export__;
});