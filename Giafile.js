var gia = exports;

// The project name and unique identifier
gia.name = 'quotes-template';
gia.guid = '730324a6-7482-4cfa-a42b-da7996c35a46';

// The root of the project, relative to http://interactive.guim.co.uk
gia.path = 'embed/2014/08/quotes-template';

gia.port = 4567;

// Libraries that should be loaded from CDN
/*gia.cdnPaths = {
	'ractive': {
		'prod': '<%= baseUrl %>/<%= gia.path %>/lib/ractive.runtime.min.js',
		'dev': '/src/vendor/ractive/ractive.runtime.min'
	}
};*/
