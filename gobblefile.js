var gobble = require( 'gobble' ),
	gia = require( './Giafile' ),
	listBowerPaths = require( 'list-bower-paths' ),


	amdPathsConfig,
	baseUrl,
	version;


amdPathsConfig = listBowerPaths.sync({ relative: true, noext: true });
baseUrl = ( gobble.isBuild ? ( 'http://interactive.guim.co.uk/' + gia.path ) : '.' );
version = 'v/' + ( true ? Date.now() : 'dev' );


module.exports = gobble([

	// preview
	gobble( 'preview' ).map( 'replace', {
		baseUrl: baseUrl,
		fonts: baseUrl + '/resources/fonts.css'
	}),

	// boot.js
	gobble( 'src' ).include( 'boot.js' ).map( 'replace', {
		amdPathsConfig: JSON.stringify( amdPathsConfig ),
		version: version
	}),

	// app.js and dependencies
	(function () {
		var app, amd, ractive_components, data, vendor;

		amd = gobble( 'src/app' ).map( 'esperanto', { type: 'amd', defaultOnly: true });
		ractive_components = gobble( 'src/ractive_components' ).map( 'ractive', { type: 'amd' }).moveTo( 'ractive_components' );

		app = gobble([ amd, ractive_components ]).map( 'es6-transpiler', {
			globals: { define: true }
		});

		data = gobble( 'src/data' ).transform( 'spelunk', { dest: 'data.js', type: 'amd' });
		vendor = gobble( 'src/vendor' );

		app = gobble([ app, data, vendor ]);

		if ( true ) {
			app = app.transform( 'requirejs', {
				name: 'app',
				out: 'app.js',
				paths: amdPathsConfig,
				optimize: 'none'
			}).map( 'amdclean', {
				wrap: {
					start: '(function(){',
					end: '\n//export\ndefine( function () { return _app_; }); }());'
				}
			});
		}

		return app.moveTo( version, 'js' );
	}()),

	// styles
	gobble( 'src/styles' ).transform( 'sass', { src: 'main.scss', dest: version + '/min.css' }),

	// files
	gobble( 'resources' ).moveTo( 'resources' )

]);
