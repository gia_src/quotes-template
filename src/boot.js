/*global define */
define( function () {

	'use strict';

	return {
		boot: function ( el, bootUrl ) {

			var supported, message, loadCSS, head, baseUrl, amdConfig, isRequireJs;

			baseUrl = bootUrl.split( '/' ).slice( 0, -1 ).join( '/' );

			amdConfig = {
				context: 'quotes-template',
				baseUrl: baseUrl + '/${version}/js',
				paths: ${amdPathsConfig}
			};

			// do feature detection etc
			supported = true;

			if ( !supported ) {
				message = '<div class="gia-browser-warning"><h2>Time to upgrade your browser!</h2><p>Your browser lacks features necessary to view this interactive. We strongly recommend upgrading to a modern browser such as <a href="http://google.com/chrome">Google Chrome</a> or <a href="http://getfirefox.com">Mozilla Firefox</a>.</p></div>';
				el.innerHTML = message;

				return;
			}

			// load CSS
			head = document.getElementsByTagName( 'head' )[0];

			loadCSS = function ( url ) {
				var link;

				// If the stylesheet already exists in the page (e.g. multiple components loading the same
				// fonts.css file), abort
				if ( document.querySelector( 'link[href="' + url + '"]' ) ) {
					return;
				}

				link = document.createElement( 'link' );
				link.setAttribute( 'rel', 'stylesheet' );
				link.setAttribute( 'href', url );

				head.appendChild( link );
			};

			loadCSS( baseUrl + '/${version}/min.css' );

			isRequireJs = typeof require() === 'function';

			if ( isRequireJs ) {
				require.config( amdConfig );
				require([ baseUrl + '/${version}/js/app.js' ], function ( app ) {
					app.launch( el );
				});
			} else {
				require( amdConfig, [ baseUrl + '/${version}/js/app' ]).then( function ( app ) {
					app.launch( el );
				});
			}


		}
	};

});
