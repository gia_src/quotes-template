import Promise from 'utils/core/Promise';

export default function get ( url, options = {} ) {
	return new Promise( function ( fulfil, reject ) {
		var xhr = new XMLHttpRequest();

		xhr.open( 'GET', url );
		if ( options.responseType ) {
			xhr.responseType = options.responseType;
		}

		xhr.onload = function () {
			fulfil( options.responseType ? xhr.response : xhr.responseText );
		};

		xhr.onerror = reject;

		xhr.send();
	});
}
