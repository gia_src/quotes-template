import data from 'data';
import BaseView from 'ractive_components/base';
import get from 'utils/ajax/get';

var app = {
	launch: function ( el ) {
		app.el = el;

		// optionally, comment this out...
		app.view = new BaseView({
			el: el
		});

		//find key
		var urlKey = window.location.search;
		var urlSlice = urlKey.slice(1, urlKey.length);
		var key = urlSlice.split('=')[1];
		
		//key = '1m4ewgtI9C8GwHO8EPn2uzaUAcHWFwcBDDWuwmTyLCYc'
		
		// figure out URL
		get( 'http://interactive.guim.co.uk/spreadsheetdata/' + key + '.json' ).then( function(json) {
			var data = JSON.parse( json );

			console.log(data)

			app.view.set({
				quotes: data.sheets.Responses,
				top: data.sheets.Header
			});
		});

		// ...and replace it with this:
		// el.innerHTML = data.scaffolding;
	}
};

window.app = app; // useful for debugging!

export default app;